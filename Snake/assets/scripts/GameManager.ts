// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
import FoodMovement from "./FoodMovement";
import SnakeMovement from "./SnakeMovement";
const {ccclass, property} = cc._decorator;

@ccclass
export default class GameManager extends cc.Component {

    @property(cc.Node)
    public endScreen: cc.Node;
    @property(cc.Node)
    food: cc.Node;
    @property(cc.Node)
    snake: cc.Node;
    @property(cc.Node)
    mainScreen: cc.Node;

    public endGame()
    {
        this.endScreen.active = true;
    }

    public tryAgain()
    {
       cc.director.loadScene("_Scenes");
    }

    public quit()
    {
        cc.game.end();
    }

}
