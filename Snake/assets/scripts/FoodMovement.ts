const {ccclass, property} = cc._decorator;

@ccclass
export default class FoodMovement extends cc.Component {


    @property 
    size: number = 50;
    @property 
    gridX: cc.Vec2 = new cc.Vec2(-13, 13);
    @property 
    gridY: cc.Vec2 = new cc.Vec2(-8, 8);

    position: cc.Vec2 = new cc.Vec2(0, 0);
    // LIFE-CYCLE CALLBACKS:

    onLoad () {

    }

    start () {
    }

    onEnterSnake(){
        this.position.x =  (Math.floor(Math.random() * (this.gridX.y - this.gridX.x + 1) ) + this.gridX.x) * this.size;
        this.position.y =  (Math.floor(Math.random() * (this.gridY.y - this.gridY.x + 1) ) + this.gridY.x) * this.size;
        this.node.setPosition(this.position);
    }

    public setDefault()
    {
        this.node.setPosition(0, -50);
    }
}
