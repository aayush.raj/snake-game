// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import FoodMovement from "./FoodMovement";
import GameManager from "./GameManager";

const {ccclass, property} = cc._decorator;

@ccclass
export default class SnakeMovement extends cc.Component {
    @property(cc.Prefab)
    public body: cc.Prefab;
    @property(cc.Integer)
    Interval: number = 0.1;
    @property(cc.Node)
    gameManager: cc.Node;
    @property(cc.Integer)
    speed: number = 50;

    start () {
        var manager = cc.director.getCollisionManager();
        manager.enabled = true;
        this.scheduleAnimation();

    }
    private moveX: number = 0;
    private moveY: number = 50;
    private segment: cc.Node[] = [];
    private direction: cc.Vec2 = new cc.Vec2(0, -400);

    onCollisionEnter(other, self)
    {
        if(other.node.group == 'food')
        {
            let food = other.node.getComponent(FoodMovement);
            food.onEnterSnake();
            this.addBody();
        }
        else
        {
            let manager = this.gameManager.getComponent(GameManager);
            manager.endGame();
            this.node.active = false;
        }
    }
    onLoad ()
    {
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN,this.onKeyDown,this);
        this.segment.push(this.node);
    }

    onKeyDown (event) {
        switch(event.keyCode) {
            case cc.macro.KEY.a:
                this.moveX = -50;
                this.moveY = 0;
                break;
            case cc.macro.KEY.d:
                this.moveX = 50;
                this.moveY = 0;
                break;
            case cc.macro.KEY.w:
                this.moveX = 0;
                this.moveY = 50;
                break;
            case cc.macro.KEY.s:
                this.moveX = 0;
                this.moveY = -50;
                break;
        }
}
    /*private doTween() {
        cc.tween(this.node).to(2,{position:new cc.Vec3(100, 100, 0)}).start();
       
    }*/

    moveSnake()
    {
        this.direction.x += this.moveX;
        this.direction.y += this.moveY;
        for(let i = (this.segment.length - 1); i > 0; i--)
        {
            this.segment[i].position = this.segment[i - 1].position;
        }
        this.node.setPosition(this.direction);

    }

    private scheduleAnimation()
    {
        this.schedule(this.moveSnake, this.Interval, cc.macro.REPEAT_FOREVER, 2);
    }

    addBody()
    {
        let part = cc.instantiate(this.body);
        part.parent = this.node.parent;
        this.segment.push(part);
    }

    public destroyBody()
    {
        this.node.active = true;
        //for(let i = (this.segment.length - 1); i > 0; i--)  
        if(this.segment.length > 0)
        {
        for(let i = 1; i < this.segment.length; i++)
        {
        let temp = this.segment[i];
        this.segment.splice(i);
        temp.destroy();
        console.log('destroyed');
        }
        }
        this.node.setPosition(0, -200);
        this.direction.x = 0;
        this.direction.y = -400;
        this.moveX = 0;
        this.moveY = 50;

    }
}
